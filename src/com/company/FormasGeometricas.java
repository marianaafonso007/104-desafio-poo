package com.company;

public abstract class FormasGeometricas {
    protected int lados;

    public double getArea() {
        return area;
    }

    protected double area;

    public abstract void calculoArea();
}
