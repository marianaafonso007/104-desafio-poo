package com.company;

public class Triangulo extends FormasGeometricas {

    private double ladoA, ladoB, ladoC;

    public Triangulo(double ladoA, double ladoB, double ladoC) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }

    public double getLadoA() {
        return ladoA;
    }

    public void setLadoA(double ladoA) {
        this.ladoA = ladoA;
    }

    public double getLadoB() {
        return ladoB;
    }

    public void setLadoB(double ladoB) {
        this.ladoB = ladoB;
    }

    public double getLadoC() {
        return ladoC;
    }

    public void setLadoC(double ladoC) {
        this.ladoC = ladoC;
    }

    public boolean verificarDados(){
        if((this.ladoA + this.ladoB) > this.ladoC && (this.ladoA + this.ladoC) > this.ladoB && (this.ladoB + this.ladoC) > this.ladoA){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void calculoArea() {
        double s = (this.ladoA + this.ladoB + this.ladoC) /2;
        this.area = Math.sqrt(s * (s - this.ladoA) * (s - this.ladoB) * (s - this.ladoB) * (s - this.ladoC));
    }

    @Override
    public String toString() {
        return "Área do triângulo calculado é = " + this.area;
    }
}
