package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Util {
    Scanner leituraDados = new Scanner(System.in);
    private Triangulo triangulo;
    private Circulo circulo;
    private Retangulo retangulo;

    public void solicitarEntradaCirculo(){
        double raio;
        mensagemUser("Informe o valor do raio:");
        raio = leituraDados.nextDouble();
        circulo = new Circulo(raio);
    }

    public void solicitarEntradaRetangulo(){
        double altura, base;
        mensagemUser("Informe a altura de seu Retângulo:");
        altura = leituraDados.nextDouble();
        mensagemUser("Informe a base de seu Retângulo:");
        base = leituraDados.nextDouble();
        retangulo = new Retangulo(altura, base);
    }

    public void solicitarEntradaTriangulo(){
        double ladoA, ladoB, ladoC;
        mensagemUser("Informe o primeiro lado do seu triangulo");
        ladoA = leituraDados.nextDouble();
        mensagemUser("Informe o segundo lado do seu triangulo");
        ladoB = leituraDados.nextDouble();
        mensagemUser("Informe o terceiro lado do seu triangulo");
        ladoC = leituraDados.nextDouble();
        triangulo = new Triangulo(ladoA, ladoB, ladoC);
    }

    public void identificarQualForma(int tipoForma) {
        if (tipoForma == 3) {
            solicitarEntradaRetangulo();
            retangulo.calculoArea();
            mensagemUser(retangulo.toString());

        } else if (tipoForma == 2) {
            solicitarEntradaTriangulo();
            if(triangulo.verificarDados()) {
                triangulo.calculoArea();
                mensagemUser(triangulo.toString());
            }else {
                mensagemUser("Você preencheu algum lado errado, solicitamos que preencha novamente os lados!");
                solicitarEntradaTriangulo();
            }
        } else {
            solicitarEntradaCirculo();
            circulo.calculoArea();
            mensagemUser(circulo.toString());
        }
    }

    public static void mensagemUser(String mensagem){
        System.out.println(mensagem);
    }

}
