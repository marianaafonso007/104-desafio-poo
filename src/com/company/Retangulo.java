package com.company;

public class Retangulo extends FormasGeometricas{

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public Retangulo(double altura, double base) {
        this.altura = altura;
        this.base = base;
    }

    private double altura;
    private double base;

    @Override
    public void calculoArea() {
        this.area = this.altura * this.base;
    }

    @Override
    public String toString() {
        return "Área do retângulo calculado é = " + this.area;
    }
}
