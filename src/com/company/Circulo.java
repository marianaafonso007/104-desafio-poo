package com.company;

public class Circulo extends FormasGeometricas {
    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }

    private double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    public void calculoArea() {
        this.area = 2 * this.raio * Math.PI;
    }

    @Override
    public String toString() {
        return "Área do circulo calculado é = " + this.area;
    }
}
