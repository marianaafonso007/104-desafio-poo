package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UI {

    Scanner leituraDados = new Scanner(System.in);
    private boolean iniciarjogo;
    private double verificarOpcaoInicio, verificarOpcaoJogo;
    Util util;

    public void inicializacao(){
        do {
            util.mensagemUser("Digite 1 para calcular a área do Círculo: ");
            util.mensagemUser("Digite 2 para calcular a área do Triângulos: ");
            util.mensagemUser("Digite 3 para calcular a área do Retângulo: ");
            util.mensagemUser("Ou digite 0 para sair: ");
            int entrada = leituraDados.nextInt();
            if (entrada > 0) {
                Util util = new Util();
                util.identificarQualForma(entrada);
            } else {
                return;
            }

            System.out.println("Deseja continuar jogando? Digite 1 para SIM e 2 para NÃO!");
            verificarOpcaoInicio = leituraDados.nextInt();
            iniciarjogo = verificarOpcaoInicio != 1 ? false : true;
        } while (iniciarjogo);
    }
}
